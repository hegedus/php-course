#
# # Comments
#
# Basic PHP comment is in fort of "Starting with # (Hash)"
# PHP is designed to work inside HTML so HTML and JavaScript comments works as well
# I Advice to use comments relative to language what is being commented.
#    We can have all 3 languages in one file and always use commits which belongs to language being commented

// This is one line comment (JavaScript)
/* This is multil
 line comment (JavaScript)/*
<!-- THis is multi
line comment (HTML) -->

# These are Tags to embed PHP inside HTML files
# Durring processing of HTML files, the code inside this tags is processed and as a result is generated text of the
# part of the HTML document
<?php  # There is also possibility to use <? or <?= as starting tags. I advice to use <?php for clarity

?>


<?php
# NOTE: PHP is whitespace agnostic language and that's why it uses semicolon(;) to distinguish commands from each other.
#       Line without trailing samicolon is considered as one command.
#       As a good best practice it's very great in whitespace agnostic languages
#         to always use semicolons at the end of the command

# NOTE: Commands can accept input data as a parameters.

# Command 'echo' takes one parameter. This parameter will be "Echoed"/"Printed"/"Returned" as a result to
#   generated HTML
echo "Hello World";

# If you would try to generate HTML code using PHP and you would use only this command,
#   There would be no output as a generated result.
#
# This command ("something inside quotes") is called 'Literal value'.
# It is a command to create particular value inside a program.
# We can use this value as a parameters (input data) for commands.
# In this case, as we do not pass this values as parameter to other command,
#   the PHP-Interpreter(a program responsible for executing this PHP embeded script) will create the Literal value
#   and as it is not used it will just "throw it away" and continue
"Hello World";

?>

# Continue on ./data_types.php
